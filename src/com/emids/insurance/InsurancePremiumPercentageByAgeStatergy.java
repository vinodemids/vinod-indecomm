package com.emids.insurance;

public class InsurancePremiumPercentageByAgeStatergy implements InsurancePremiumStatergy {
	@Override
	public double calculatePremiumPercentage(Person person, double premium, InsurancePremiumExpression premiumCalculation) {
		int age = person.getAge();
		
		premium = premiumCalculation.calculatePremiumAmount(premium, 10 );
		if  ( age>=18 && age<25) {
			return premium;
		}
		
		premium = premiumCalculation.calculatePremiumAmount(premium, 10 );
		if  ( age>=25 && age<30) {
			return premium;
		}
		
		premium = premiumCalculation.calculatePremiumAmount(premium, 10 );
		if  ( age>=30 && age<35) {
			return premium;
		}
		
		premium = premiumCalculation.calculatePremiumAmount(premium, 10 );
		if  ( age>=35 && age<40) {
			return premium;
		}
		
		return premiumCalculation.calculatePremiumAmount(premium, 20 );
	}

}
