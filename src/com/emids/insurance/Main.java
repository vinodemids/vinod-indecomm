package com.emids.insurance;

import com.emids.insurance.common.builders.PersonBuilder;

public class Main {

	public static void main(String[] args) {
		PersonBuilder personBuilder = new PersonBuilder();
		Person person = personBuilder.build();
		double premium = InsurancePremiumCalculator.calculatePremium(person);
		System.out.println("Premium Is : " + premium);
	}

}
