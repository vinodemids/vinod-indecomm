package com.emids.insurance;

public interface InsurancePremiumExpression {
	double calculatePremiumAmount(double premiumAmount, int percentage);
}
