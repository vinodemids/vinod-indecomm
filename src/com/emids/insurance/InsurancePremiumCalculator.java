package com.emids.insurance;

public class InsurancePremiumCalculator {

	private static int basicPremiumAge = 18;
	private static double premium = 5000.0;
	
	private static InsurancePremiumExpression premiumCalculation = 
			(premiumAmount, percentage) ->  (premiumAmount + ((premiumAmount*percentage)/100));
	
	public static double calculatePremium(Person person) {
		if( person.getAge() < basicPremiumAge ) {
			return premium;
		}
		InsurancePremiumStatergy insurancePremiumStatergy = InsurancePremiumStatergyFactory.getCalculatePercentageByAge();
		premium = insurancePremiumStatergy.calculatePremiumPercentage(person, premium, premiumCalculation);
		
		insurancePremiumStatergy = InsurancePremiumStatergyFactory.getCalculatePercentageByGender();
		premium = insurancePremiumStatergy.calculatePremiumPercentage(person, premium, premiumCalculation);
		
		insurancePremiumStatergy = InsurancePremiumStatergyFactory.getCalculatePercentageByHealthAndHobbit();
		premium = insurancePremiumStatergy.calculatePremiumPercentage(person, premium, premiumCalculation);
		
		return premium;
	}
	
}
