package com.emids.insurance;

public class InsurancePremiumStatergyFactory {

	private static InsurancePremiumPercentageByHealthAndHobbitStatergy calculatePercentageByHealthAndHobbit;
	private static InsurancePremiumPercentageByAgeStatergy calculatePercentageByAge;
	private static InsurancePremiumPercentageByGenderStatergy calculatePercentageByGender;
	
	public InsurancePremiumStatergyFactory() {
	}
	
	public static InsurancePremiumStatergy getCalculatePercentageByHealthAndHobbit() {
		if  ( calculatePercentageByHealthAndHobbit == null) {
			calculatePercentageByHealthAndHobbit = new InsurancePremiumPercentageByHealthAndHobbitStatergy();
		}
		return calculatePercentageByHealthAndHobbit;
	}
	
	public static InsurancePremiumStatergy getCalculatePercentageByAge() {
		if  ( calculatePercentageByAge == null) {
			calculatePercentageByAge = new InsurancePremiumPercentageByAgeStatergy();
		}
		return calculatePercentageByAge;
	}
	
	public static InsurancePremiumStatergy getCalculatePercentageByGender() {
		if  ( calculatePercentageByGender == null) {
			calculatePercentageByGender = new InsurancePremiumPercentageByGenderStatergy();
		}
		return calculatePercentageByGender;
	}
}
