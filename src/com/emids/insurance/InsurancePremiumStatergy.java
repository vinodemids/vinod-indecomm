package com.emids.insurance;

public interface InsurancePremiumStatergy {

	double calculatePremiumPercentage(Person person, double premium, InsurancePremiumExpression premiumCalculation);
	
}
