package com.emids.insurance;

public class InsurancePremiumPercentageByHealthAndHobbitStatergy implements InsurancePremiumStatergy {

	@Override
	public double calculatePremiumPercentage(Person person, double premium, InsurancePremiumExpression premiumCalculation) {
		return premiumCalculation.calculatePremiumAmount(premium, person.getNumber() );
	}

}
