package com.emids.insurance;

public class InsurancePremiumPercentageByGenderStatergy implements InsurancePremiumStatergy {

	@Override
	public double calculatePremiumPercentage(Person person, double premium, InsurancePremiumExpression premiumCalculation) {
		char gender = person.getGender();
		switch ( gender ) {
		case 'M':
			return premiumCalculation.calculatePremiumAmount(premium, person.getNumber() );

		default:
			return premium;
		}

		
	}
}
