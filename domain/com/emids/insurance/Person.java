package com.emids.insurance;

import java.util.ArrayList;
import java.util.List;

public class Person {

	private String name;
	private int  age;
	private char gender;
	
	private int number= 0; 
	
	private List<PersonHabbit> personHabbits = new ArrayList<PersonHabbit>();
	private List<PersonHealth> personHealths = new ArrayList<PersonHealth>();
	
	public Person() {
		super();
	}

	public Person(String name, int age, char gender, List<PersonHabbit> personHabbits,
			List<PersonHealth> personHealths) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.personHabbits = personHabbits;
		this.personHealths = personHealths;
		
		
		for ( PersonHabbit habbit : personHabbits ) {
			if  ( habbit.getIsGoodHabit() )  {
				number -= 3;
			} else {
				number += 3;
			}
		}
		
		for ( PersonHealth health : personHealths ) {
			if  ( health.getIsAffected() )  {
				number -= 1;
			} else {
				number += 1;
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public List<PersonHabbit> getPersonHabbits() {
		return personHabbits;
	}

	public void setPersonHabbits(List<PersonHabbit> personHabbits) {
		this.personHabbits = personHabbits;
	}

	public List<PersonHealth> getPersonHealths() {
		return personHealths;
	}

	public void setPersonHealths(List<PersonHealth> personHealths) {
		this.personHealths = personHealths;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender 
					+ ", personHabbits=" + personHabbits
						+ ", personHealths=" + personHealths + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + gender;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personHabbits == null) ? 0 : personHabbits.hashCode());
		result = prime * result + ((personHealths == null) ? 0 : personHealths.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (gender != other.gender)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personHabbits == null) {
			if (other.personHabbits != null)
				return false;
		} else if (!personHabbits.equals(other.personHabbits))
			return false;
		if (personHealths == null) {
			if (other.personHealths != null)
				return false;
		} else if (!personHealths.equals(other.personHealths))
			return false;
		return true;
	}
}
