package com.emids.insurance;


public class PersonHabbit {
	private String name;
	private Boolean isGoodHabit;
	
	public PersonHabbit(String name, Boolean isGoodHabit) {
		super();
		this.name = name;
		this.isGoodHabit = isGoodHabit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsGoodHabit() {
		return isGoodHabit;
	}

	public void setIsGoodHabit(Boolean isGoodHabit) {
		this.isGoodHabit = isGoodHabit;
	}

	
}
