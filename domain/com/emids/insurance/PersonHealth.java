package com.emids.insurance;

public class PersonHealth {
	private String name;
	private Boolean isAffected;
	
	public PersonHealth(String name, Boolean isAffected) {
		super();
		this.name = name;
		this.isAffected = isAffected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsAffected() {
		return isAffected;
	}

	public void setIsAffected(Boolean isAffected) {
		this.isAffected = isAffected;
	}
	
	

}