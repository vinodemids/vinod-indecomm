package com.emids.insurance.common.builders;

import com.emids.insurance.Person;

public class PersonBuilder {

	private Person person = new Person();
	
	public PersonBuilder() {
		person.setAge(34);
		person.setName("ABC");
		person.setGender('M');
	}
	
	public PersonBuilder withAge(int age) {
		person.setAge(age);
		return this;
	}
	
	public PersonBuilder withGender(char gender) {
		person.setGender(gender);
		return this;
	}
	
	public Person build() {
		return person;
	}
}
